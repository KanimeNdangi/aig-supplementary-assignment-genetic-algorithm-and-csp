### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 6b6210c4-c7c1-42fa-a7e1-cbd0bbf5d90a
using Test

# ╔═╡ 17d2abb1-392a-4768-9014-430531212b98
using PlutoUI

# ╔═╡ afd639a0-dfe1-11eb-2353-2f33a4d3fea8
md"## Genetic Algorithm Implementation"

# ╔═╡ 98d0b0b5-6cf6-4b24-aca4-88a95a64f538
md"### Abstract Type Definition"

# ╔═╡ 915127af-4b80-447e-8e9f-f0b7b3de2c97
#abstract type AbstractOptimizer end

# ╔═╡ 3f4c9002-f235-4559-bc91-de7998d7486a
abstract type AbstractOptimizer end
	

# ╔═╡ 000a007c-8eee-457f-a59a-a73d3b9597be
md"### Genetic Algorithm Definition"

# ╔═╡ 119345db-10d5-4a25-86ce-eb00f73f6efd
md"### Population Definition"

# ╔═╡ bd36ef8a-97d2-483d-bac0-e3d1d6325d59
md"### Fitness Function"

# ╔═╡ 0e0ec3ce-ffe6-4e6a-9496-55397c31a97e
abstract type Fitness end

# ╔═╡ 9e50acd3-45f8-4fb7-b60e-d8ac93be922e
struct FitnessFunc{T <: Number} <: Fitness
  maximise::Bool
  minimise::Bool

  FitnessFunc(m::Bool) = new{T}(m)
end

# ╔═╡ 367d9fad-4388-4c7b-8bfe-a358c4371a5c
abstract type AbstractOptState end

# ╔═╡ 0eca6013-1d62-42b7-b885-d78dae78d796
mutable struct GAState{T,IT} <: AbstractOptState
    N::Int
    eliteSize::Int
    fitness::T
    fitpop::Vector{T}
    fittest::IT
end

# ╔═╡ da04ee8c-38e2-4542-a05c-2af3273f29ac
value(s::GAState) = s.fitness

# ╔═╡ c7089090-bd87-4d0f-bca7-678f73957bc1
minimizer(s::GAState) = s.fittest

# ╔═╡ 9dd1f986-bc49-4c23-8364-00b88cafc73d
md"### Individual/ Gene Definition"

# ╔═╡ a3d4e8f7-c74b-47cc-955f-79eafa4db3bf

#Returns the same parameter individuals `v1` and `v2` as an offspring pair.
identity(v1::T, v2::T) where {T <: AbstractVector} = (v1,v2)


# ╔═╡ 423a285f-ddef-401a-a14b-5754db161ef8
mutable struct GeneticAlgorithm <: AbstractOptimizer
    populationSize::Int
    selectionProb::Float64
    mutationProb::Float64
    i::Real
    selection::Function
    crossover::Function
    mutation::Function

    GeneticAlgorithm(; populationSize::Int=50, selectionProb::Float64=0.8, mutationProb::Float64=0.1,
        i::Real=0, gene::Real=i,
        selection::Function = ((x,n)->1:n),
        crossover::Function = identity, mutation::Function = identity) =
        new(populationSize, selectionProb, mutationProb, gene, selection, crossover, mutation)
end

# ╔═╡ bf1121e6-2bd3-4bb3-947f-00f67b852957
population_size(method::GeneticAlgorithm) = method.populationSize

# ╔═╡ 73fea65e-4d5f-4464-93e4-37d3d6d3e22a
default_options(method::GeneticAlgorithm) = (iterations=1000, abstol=1e-15)

# ╔═╡ 3e796c1b-80b4-4e46-91da-5425ad39d45d
summary(m::GeneticAlgorithm) = "GA[P=$(m.populationSize),x=$(m.selectionProb),μ=$(m.mutationProb),ɛ=$(m.i)]"

# ╔═╡ 00045c3a-905f-4a5b-81b2-fdf6d261a433
"""Initialization of Genetic Algorithm  state"""
function initial_state(method::GeneticAlgorithm, options, objfun, population)
    T = typeof(value(objfun))
    N = length(first(population))
    fitness = zeros(T, method.populationSize)

    # setup state values
    eliteSize = isa(method.i, Int) ? method.i : round(Int, method.i * method.populationSize)

    # Evaluate population fitness
    fitness = map(i -> value(objfun, i), population)
    minfit, fitidx = findmin(fitness)

    # setup initial state
    return GAState(N, eliteSize, minfit, fitness, copy(population[fitidx]))
end

# ╔═╡ 3bd22359-6d2c-4215-a5d8-7d68a930c19a


# ╔═╡ d697f691-15f5-4887-87d2-a4a2c5bfdd96
md"### Selection Probability Function Definition"

# ╔═╡ 189ff39a-f1f5-44af-8e67-310d348ca122
# Utils: selection
function pselection(prob::Vector{<:Real}, N::Int)
    cp = cumsum(prob)
    # println(cp)
    selected = Array{Int}(undef, N)
    for i in 1:N
        j = 1
        r = rand()
        while cp[j] < r
            j += 1
        end
        selected[i] = j
    end
    return selected
end

# ╔═╡ 45ed0f56-8bdc-4c19-92c3-ae0867c047e9
function roulette(fitness::Vector{<:Real}, N::Int)
    absf = abs.(fitness)
    prob = absf./sum(absf)
    return pselection(prob, N)
end

# ╔═╡ f234d4c7-e128-4639-8857-c2300d202655
rouletteinv(fitness::Vector{<:Real}, N::Int) = roulette(1.0 ./ fitness, N)

# ╔═╡ 5feb703e-6f71-4ce6-9d2f-93da2b9228bf
function randexcl(itr, exclude, dims)
    idxs = Int[]
    while length(idxs) < dims
        j = rand(itr)
        (j ∈ exclude || j ∈ idxs) && continue
        push!(idxs, j)
    end
    return idxs
end

# ╔═╡ 8e251766-3612-46cf-b338-abf6fc83d111
md"### Crossover Point Function Definition"

# ╔═╡ 82314286-b12e-47c6-bf6c-1e7ad2c38273
abstract type Crossover end

# ╔═╡ eb350d43-1203-450a-be66-ff5a6c175282
struct OnePointCrossover{R} <: Crossover
  stage::AbstractString
  rate::Float64

  OnePointCrossover(stage::AbstractString, rate::Float64) =
    new{T}(stage, rate)
end

# ╔═╡ 1d9fb4a0-aa79-4851-a15f-78a31526c048
begin
	"""
	Used for composing one point crossover operators.
	"""
	function onepoint(v1::T, v2::T) where {T <: AbstractVector}
	  stage::AbstractString
	  rate::Float64
	end
	  
	  onepoint() = new("", 0.7)
	  #onepoint(r::Float64) = new{T}("", r)
	  onepoint(s::AbstractString) = new(s, 0.7)
	  onepoint(s::AbstractString, r::Float64) = new(s, r)
	
end

# ╔═╡ 108a4121-5cde-43ff-ba11-ed9e919c891c
function vswap!(v1::T, v2::T, idx::Int) where {T <: AbstractVector}
    val = v1[idx]
    v1[idx] = v2[idx]
    v2[idx] = val
end

# ╔═╡ 1946665a-be0a-45d7-887f-7fd8c0689438
"""
Single point crossover between `v1` and `v2` individuals.
"""
function singlepoint(v1::T, v2::T) where {T <: AbstractVector}
    l = length(v1)
    c1 = copy(v1)
    c2 = copy(v2)
    pos = rand(1:l)
    for i in pos:l
        vswap!(c1, c2, i)
    end
    return c1, c2
end


# ╔═╡ 7fd5f335-4049-4d0c-b214-fcba9860eace
function inmap(v::T, c::AbstractVector{T}, from::Int, to::Int) where {T}
    exists = 0
    for j in from:to
        if exists == 0 && v == c[j]
            exists = j
        end
    end
    return exists
end

# ╔═╡ 3b1851be-d379-4b43-be27-30dec1944ee6
#N = []

# ╔═╡ 56d90dc2-fcda-43de-bdbc-a7d125190518
md"### Mutation Probability Function"

# ╔═╡ db162634-2dd8-4c43-823a-0cd171e5ead4
"""
    flip(recombinant)

Returns an in-place mutated binary `recombinant` with a bit flips at random positions.
"""
function flip(recombinant::T) where {T <: BitVector}
    s = length(recombinant)
    pos = rand(1:s)
    recombinant[pos] = !recombinant[pos]
    return recombinant
end


# ╔═╡ 2a419bec-8522-4476-85b2-739efeb59358
# Utilities
# =====
function swap!(v::T, from::Int, to::Int) where {T <: AbstractVector}
    val = v[from]
    v[from] = v[to]
    v[to] = val
end

# ╔═╡ 3dd1c31f-ac35-4d6d-b8bb-b5fc3bd63ee1
function inversion(recombinant::T) where {T <: AbstractVector}
    l = length(recombinant)
    from, to = rand(1:l, 2)
    from, to = from > to ? (to, from)  : (from, to)
    l = round(Int,(to - from)/2)
    for i in 0:(l-1)
        swap!(recombinant, from+i, to-i)
    end
    return recombinant
end

# ╔═╡ eef08e71-4830-4992-b7eb-f764ca27b4e8
abstract type AbstractStrategy end

# ╔═╡ b79cc950-e44d-4bb4-8c00-5f4ab6083f5b
function mutationwrapper(gamutation::Function)
    wrapper(recombinant::T, s::S) where {T <: AbstractVector, S <: AbstractStrategy} =  gamutation(recombinant)
    return wrapper
end

# ╔═╡ f91c5425-a3b5-4464-9036-5810544accfd
md"### Optimization Functions"

# ╔═╡ 6b3e2d6a-beb3-409a-8820-148afcc960e7
function print_header(method::AbstractOptimizer)
	    println("Iter     Function value")
end

# ╔═╡ fb5cc77f-1918-4752-b856-936b98e31c70
after_while!(objfun, state, method, options) = nothing

# ╔═╡ 5b6edbae-bb88-4be3-a713-34fb48b9fda0
abstract type AbstractObjective end

# ╔═╡ 24863a0c-d637-43d0-aeae-6018539dabfa
abstract type AbstractConstraints end

# ╔═╡ 1b3035e8-1ff6-4d48-8458-85dfcbddc0e5
md"### Optimizer"

# ╔═╡ 894f7370-5a58-48c4-af9c-3535dc824722
abstract type AbstractObjecive end

# ╔═╡ 06f9d2ca-94b8-4cf4-b2c1-8b5c3125d811
abstract type AbstractCoonstraints end

# ╔═╡ e4923383-5284-41b8-9058-6d05439e9865
population_size(method::AbstractOptimizer) = error("`population_size` is not implemented for $(summary(method)).")

# ╔═╡ e34bb95e-a811-471f-b362-7c138b82be3f
# Optimization interface
function optimize(f, lower, upper, method::M,
                  options::Options = Options(;default_options(method)...)
                 ) where {M<:AbstractOptimizer}
    bounds = ConstraintBounds(lower,upper,[],[])
    optimize(f, bounds, method, options)
end

# ╔═╡ 96307a73-b7a6-42f9-8359-1e39a85172ed
function optimize(f, lower, upper, individual, method::M,
                  options::Options = Options(;default_options(method)...)
                 ) where {M<:AbstractOptimizer}
    optimize(f, BoxConstraints(lower,upper), individual, method, options)
end

# ╔═╡ 71872daa-0823-42a3-9536-e98c14e47e82
md"### Crossover Mating, Offspring Selection, Mutation and New Population Generation"

# ╔═╡ 447d93a2-a97d-4527-b1da-5192cdd8e852
function update_state!(objfun, constraints, state, population::AbstractVector{IT}, method::GeneticAlgorithm, itr) where {IT}
   populationSize,selectionProb,mutationProb,ɛ,selection,crossover,mutation = method

    offspring = similar(population)

    # Select offspring
    selected = selection(state.fitpop, populationSize)

    # Perform mating
    offidx = randperm(populationSize)
    offspringSize = populationSize - state.eliteSize
    for i in 1:2:offspringSize
        j = (i == offspringSize) ? i-1 : i+1
        if rand() < crossoverRate
            offspring[i], offspring[j] = crossover(population[selected[offidx[i]]], population[selected[offidx[j]]])
        else
            offspring[i], offspring[j] = population[selected[i]], population[selected[j]]
        end
    end

    # Elitism (copy population individuals before they pass to the offspring & get mutated)
    fitidxs = sortperm(state.fitpop)
    for i in 1:state.eliteSize
        subs = offspringSize+i
        offspring[subs] = copy(population[fitidxs[i]])
    end

    # Perform mutation
    for i in 1:offspringSize
        if rand() < mutationRate
            mutation(offspring[i])
        end
    end

    # Create new generation & evaluate it
    for i in 1:populationSize
        o = apply!(constraints, offspring[i])
        population[i] = o
        state.fitpop[i] = value(objfun, o)
    end
    # apply penalty to fitness
    penalty!(state.fitpop, constraints, population)

    # find the best individual
    minfit, fitidx = findmin(state.fitpop)
    state.fittest = population[fitidx]
    state.fitness = state.fitpop[fitidx]

    return false
end

# ╔═╡ e2857a5c-40fa-428e-b805-5f4ad883bac8
md"### Testing... "

# ╔═╡ 39f58a5a-72f8-4639-b45a-c8f6fc622929
md"### NQueens Genetic Algorithm Test"

# ╔═╡ 7f03382b-5f7f-4ada-bc41-b19dec696604
#@testset "n-Queens"

# ╔═╡ fa3dd53e-f4e7-4114-983e-a04004f60e11
N = 8

# ╔═╡ fb6d7895-6606-4917-b2ac-7a307b620e69
P = 100

# ╔═╡ 48c80f65-adfe-4a4e-b781-668345fc467a
generatePositions = ()->collect(1:N)[randperm(N)]

# ╔═╡ 1ed5e001-e5f5-4031-9e28-2689f76e5a34
# Vector of N cols filled with numbers from 1:N specifying row position
    function nqueens(queens::Vector{Int})
        n = length(queens)
        fitness = 0
        for i=1:(n-1)
            for j=(i+1):n
                k = abs(queens[i] - queens[j])
                if (j-i) == k || k == 0
                    fitness += 1
                end
                 println("$(i),$(queens[i]) <=> $(j),$(queens[j]) : $(fitness)")
            end
        end
        return fitness
    end

# ╔═╡ 2e791f1d-d4d0-4fee-b5ee-01fb6ba751d2
@test nqueens([2,4,1,3]) == 0

# ╔═╡ d545864a-e376-4184-bc6d-ba7f23db1f5d
@test nqueens([3,1,2]) == 1

# ╔═╡ cae2c977-1fc6-4ef3-b85a-8c74e114d6f3
# Testing: Genetic Algorith solution with various mutations
    
            GeneticAlgorithm(
                populationSize = P,
                selection = rouletteinv,
                crossover = singlepoint,
                mutation = inversion,
			    mutationProb = 0.2,
                selectionProb = 0.5,
                i = 0.1, 
            )

# ╔═╡ 20c2a814-e559-4e41-9a8a-4cc9111cf0bd
with_terminal() do 
	println("Best Solutions", @test nqueens([2,4,1,3]) == 0)
end

# ╔═╡ 987b60e0-97c7-4343-8890-85748c102499
with_terminal() do 
	println("Best Solutions", @test nqueens([3,1,2]) == 1)
end

# ╔═╡ 8c6b96f4-3b92-4e74-845b-add629137430
md"#### End of Assignment"

# ╔═╡ Cell order:
# ╟─afd639a0-dfe1-11eb-2353-2f33a4d3fea8
# ╟─98d0b0b5-6cf6-4b24-aca4-88a95a64f538
# ╠═915127af-4b80-447e-8e9f-f0b7b3de2c97
# ╠═3f4c9002-f235-4559-bc91-de7998d7486a
# ╠═000a007c-8eee-457f-a59a-a73d3b9597be
# ╠═423a285f-ddef-401a-a14b-5754db161ef8
# ╠═119345db-10d5-4a25-86ce-eb00f73f6efd
# ╠═bf1121e6-2bd3-4bb3-947f-00f67b852957
# ╠═73fea65e-4d5f-4464-93e4-37d3d6d3e22a
# ╠═3e796c1b-80b4-4e46-91da-5425ad39d45d
# ╠═bd36ef8a-97d2-483d-bac0-e3d1d6325d59
# ╠═0e0ec3ce-ffe6-4e6a-9496-55397c31a97e
# ╠═9e50acd3-45f8-4fb7-b60e-d8ac93be922e
# ╠═367d9fad-4388-4c7b-8bfe-a358c4371a5c
# ╠═0eca6013-1d62-42b7-b885-d78dae78d796
# ╠═da04ee8c-38e2-4542-a05c-2af3273f29ac
# ╠═c7089090-bd87-4d0f-bca7-678f73957bc1
# ╠═00045c3a-905f-4a5b-81b2-fdf6d261a433
# ╠═9dd1f986-bc49-4c23-8364-00b88cafc73d
# ╠═a3d4e8f7-c74b-47cc-955f-79eafa4db3bf
# ╠═3bd22359-6d2c-4215-a5d8-7d68a930c19a
# ╠═d697f691-15f5-4887-87d2-a4a2c5bfdd96
# ╠═45ed0f56-8bdc-4c19-92c3-ae0867c047e9
# ╠═f234d4c7-e128-4639-8857-c2300d202655
# ╠═189ff39a-f1f5-44af-8e67-310d348ca122
# ╠═5feb703e-6f71-4ce6-9d2f-93da2b9228bf
# ╠═8e251766-3612-46cf-b338-abf6fc83d111
# ╠═82314286-b12e-47c6-bf6c-1e7ad2c38273
# ╠═eb350d43-1203-450a-be66-ff5a6c175282
# ╠═1d9fb4a0-aa79-4851-a15f-78a31526c048
# ╠═6b6210c4-c7c1-42fa-a7e1-cbd0bbf5d90a
# ╠═1946665a-be0a-45d7-887f-7fd8c0689438
# ╠═108a4121-5cde-43ff-ba11-ed9e919c891c
# ╠═7fd5f335-4049-4d0c-b214-fcba9860eace
# ╠═3b1851be-d379-4b43-be27-30dec1944ee6
# ╠═56d90dc2-fcda-43de-bdbc-a7d125190518
# ╠═db162634-2dd8-4c43-823a-0cd171e5ead4
# ╠═3dd1c31f-ac35-4d6d-b8bb-b5fc3bd63ee1
# ╠═2a419bec-8522-4476-85b2-739efeb59358
# ╠═eef08e71-4830-4992-b7eb-f764ca27b4e8
# ╠═b79cc950-e44d-4bb4-8c00-5f4ab6083f5b
# ╠═f91c5425-a3b5-4464-9036-5810544accfd
# ╠═6b3e2d6a-beb3-409a-8820-148afcc960e7
# ╠═fb5cc77f-1918-4752-b856-936b98e31c70
# ╠═5b6edbae-bb88-4be3-a713-34fb48b9fda0
# ╠═24863a0c-d637-43d0-aeae-6018539dabfa
# ╠═1b3035e8-1ff6-4d48-8458-85dfcbddc0e5
# ╠═894f7370-5a58-48c4-af9c-3535dc824722
# ╠═06f9d2ca-94b8-4cf4-b2c1-8b5c3125d811
# ╠═e4923383-5284-41b8-9058-6d05439e9865
# ╠═e34bb95e-a811-471f-b362-7c138b82be3f
# ╠═96307a73-b7a6-42f9-8359-1e39a85172ed
# ╟─71872daa-0823-42a3-9536-e98c14e47e82
# ╠═447d93a2-a97d-4527-b1da-5192cdd8e852
# ╠═e2857a5c-40fa-428e-b805-5f4ad883bac8
# ╠═39f58a5a-72f8-4639-b45a-c8f6fc622929
# ╠═7f03382b-5f7f-4ada-bc41-b19dec696604
# ╠═fa3dd53e-f4e7-4114-983e-a04004f60e11
# ╠═fb6d7895-6606-4917-b2ac-7a307b620e69
# ╠═48c80f65-adfe-4a4e-b781-668345fc467a
# ╠═1ed5e001-e5f5-4031-9e28-2689f76e5a34
# ╠═17d2abb1-392a-4768-9014-430531212b98
# ╠═2e791f1d-d4d0-4fee-b5ee-01fb6ba751d2
# ╠═d545864a-e376-4184-bc6d-ba7f23db1f5d
# ╠═cae2c977-1fc6-4ef3-b85a-8c74e114d6f3
# ╠═20c2a814-e559-4e41-9a8a-4cc9111cf0bd
# ╠═987b60e0-97c7-4343-8890-85748c102499
# ╠═8c6b96f4-3b92-4e74-845b-add629137430
