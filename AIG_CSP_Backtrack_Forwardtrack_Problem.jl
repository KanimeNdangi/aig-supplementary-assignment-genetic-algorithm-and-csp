### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ b350aeaf-dda4-4d5e-868d-1ea92b408dbb
using PlutoUI

# ╔═╡ faacd953-6f37-4269-8ac4-d9703df190ca
md"## CSP Forward and Backtracking Search Imlementation"

# ╔═╡ 30df6be8-0420-4fd5-9723-49e487f8b8f8
md"### Enumerator for Dmain Network"

# ╔═╡ fe219ba8-47bf-4bc6-9e0a-4749869ab669
 @enum DomainNetwork begin 
	one = 1
	two = 2
	three = 3
	four = 4
end

# ╔═╡ 104d6460-e9d2-479b-80d3-031833fda095
md"### Network Variable and Constraint Definitions"

# ╔═╡ ae0406c0-df96-11eb-05bd-ddaa8d38de68
mutable struct NetworkVariable
	name::String
    value::Union{Nothing,DomainNetwork}
    restricted_values::Vector{DomainNetwork}
    domain_restriction_number::Int64
end

# ╔═╡ 309c9f0a-d9d2-4bae-b95e-5846f4c04641
struct NetworkConstraints
var::Vector{NetworkVariable}
constraints::Vector{Tuple{NetworkVariable,NetworkVariable}}
end

# ╔═╡ 72add19e-a069-4d77-8609-af80306c839c
net = rand(setdiff(Set([one,two,three,four]), Set([one,four])))

# ╔═╡ 0a35cd76-89d7-4ff0-ae5a-1608cf86a413
md"### Forward and Backtracking Function"

# ╔═╡ cc8c5841-b420-4272-a865-9c5d2c8638d8
function csp(net::NetworkConstraints, all_value_assignment)
	for current_variable in net.var
		if current_variable.domain_restriction_number == 4
			return []
			else
			following_value = rand(setdiff(Set([one,two,three,four]),
					Set(current_variable.restricted_values)))
			#assign the value to the variable
			current_variable.value = following_value
       
			#forward tracking
			for current_constraint in net.constraints
				if !((current_constraint[1] == current_variable) || 
						(current_constraint[2] == current_variable))
					continue
					else
					if current_constraint[1] == current_variable
						push!(current_constraint[2].restricted_values, following_value)
						current_constraint[2].domain_restriction_number += 1
					 # Backward tracking
                     # when the number reached is maximum then go backwards
                     # else continue moving forward 
						else
						push!(current_constraint[1].restricted_values, following_value)
current_constraint[1].domain_restriction_number += 1
#                       else continue moving forward 
						end
					end
				end
               # add the assignment to all_val_assigns
			push!(all_value_assignment, current_variable.name => following_value)
			end
		end
	return all_value_assignment
	return all_value_assignment
end


# ╔═╡ 0c1d659e-e661-401c-b00a-02c1c43b1b19
md"### Variable to Constraint assignment"

# ╔═╡ bd3d1acc-40d1-4764-8810-ca5e55dfe9d6
x1 = NetworkVariable("X1",nothing, [], 0)

# ╔═╡ a09cda0e-4a6c-40a9-ae92-f335c6d88888
x2 = NetworkVariable("X2", nothing, [], 0)

# ╔═╡ 7e11e9fd-697a-4342-a411-120150a705ce
x3 = NetworkVariable("X3", nothing, [], 0)

# ╔═╡ 78c47d90-87ea-43b4-ac96-4286432198df
x4 = NetworkVariable("X4", nothing, [], 0)

# ╔═╡ 30a4930f-2c6e-452a-b290-18f16cfbcb0a
x5 = NetworkVariable("X5", nothing, [], 0)

# ╔═╡ b5a889e4-831a-42e4-90ed-b4593770be25
x6 = NetworkVariable("X6", nothing, [], 0)

# ╔═╡ 0ffbaed8-8b39-475a-a3a6-fa9b4fb02dc7
x7 = NetworkVariable("X7", nothing, [], 0)

# ╔═╡ f8b3e377-dde4-411a-9dd9-9fb8ee4a341b
md"### Naive Inference method"

# ╔═╡ a98e1369-1156-428e-953e-9ab2cbe7f7ca
network = NetworkConstraints([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ 601d51e0-735d-46a7-b611-1e551290e042
md"### Backtracking and Forward checking"

# ╔═╡ 14ceae69-326f-448b-af74-915b1bc830ec
csp(network, [])

# ╔═╡ 087ddd45-59df-4010-8e3a-5d6f14885a21
with_terminal() do
	println(csp(network,[]))
end

# ╔═╡ 6aa5dba1-4aa1-4697-8fc1-87ab3caa60e6
md"#### End of Assignment"

# ╔═╡ Cell order:
# ╠═faacd953-6f37-4269-8ac4-d9703df190ca
# ╠═30df6be8-0420-4fd5-9723-49e487f8b8f8
# ╠═fe219ba8-47bf-4bc6-9e0a-4749869ab669
# ╠═104d6460-e9d2-479b-80d3-031833fda095
# ╠═ae0406c0-df96-11eb-05bd-ddaa8d38de68
# ╠═309c9f0a-d9d2-4bae-b95e-5846f4c04641
# ╠═72add19e-a069-4d77-8609-af80306c839c
# ╠═0a35cd76-89d7-4ff0-ae5a-1608cf86a413
# ╠═cc8c5841-b420-4272-a865-9c5d2c8638d8
# ╠═0c1d659e-e661-401c-b00a-02c1c43b1b19
# ╠═bd3d1acc-40d1-4764-8810-ca5e55dfe9d6
# ╠═a09cda0e-4a6c-40a9-ae92-f335c6d88888
# ╠═7e11e9fd-697a-4342-a411-120150a705ce
# ╠═78c47d90-87ea-43b4-ac96-4286432198df
# ╠═30a4930f-2c6e-452a-b290-18f16cfbcb0a
# ╠═b5a889e4-831a-42e4-90ed-b4593770be25
# ╠═0ffbaed8-8b39-475a-a3a6-fa9b4fb02dc7
# ╠═f8b3e377-dde4-411a-9dd9-9fb8ee4a341b
# ╠═b350aeaf-dda4-4d5e-868d-1ea92b408dbb
# ╠═a98e1369-1156-428e-953e-9ab2cbe7f7ca
# ╠═601d51e0-735d-46a7-b611-1e551290e042
# ╠═14ceae69-326f-448b-af74-915b1bc830ec
# ╠═087ddd45-59df-4010-8e3a-5d6f14885a21
# ╟─6aa5dba1-4aa1-4697-8fc1-87ab3caa60e6
